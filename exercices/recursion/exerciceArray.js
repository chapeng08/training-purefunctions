/* Reverse Array

Ecrivez une fonction qui accepte un tableau et retourne une copie inversée de ce
tableau. Utilisez la recursion.

*/

var arr = [1,2,3,4]
var reversedArr = reverseArray(arr);
function reverseArray(arr){
  var revArr = [];
  if(arr.length === 0){
    return revArr;
}
  else {
    revArr.push(arr.pop());
    console.log(revArr);
    return revArr.concat(reverseArray(arr));
  }
}
  console.log(reversedArr)    // [4,3,2,1]
console.log(arr)    // [1,2,3,4]
