/* Fibonacci

Créez une fonction qui prend un nombre n et retourne le nième nombre dans la
Séquence de Fibonacci. La séquence de Fibonacci est une série de nombres dans
lesquels chaque nombre est la somme des deux nombres qui le précèdent.

ex: 1, 1, 2, 3, 5, 8, 13, 21, 34, 55....

*/


function fibonacci(n){
//base case
  if(n<2){
    return 1;
}

  else{
    return fibonacci(n-2) + fibonacci(n-1);
}
}

// fibonacci(3);
