/* Compter les voyelles

Ecrivez une fonction qui accepte une chaîne et renvoie le nombre de voyelles
dans cette chaîne. Utilisez la recursion.

*/


function countVowels(string){
  string = string.toLowerCase(); // base case
  if(string.length === 0){
    return 0;
}
  if(string[0] === "a" || string[0] === "e" || string[0] === "i" || string[0] === "o" || string[0] === "u"){
    return 1 + countVowels(string.slice(1));
}
  else{
    return countVowels(string.slice(1));
}
}

// countVowels("Four score and seven years")
