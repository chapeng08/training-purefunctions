#### Introduction : #

La récursion est un modèle de programmation utile dans les situations où une
tâche peut être naturellement divisée en plusieurs tâches du même type, mais
plus simple. Ou lorsqu'une tâche peut être simplifiée en une action facile plus
une variante plus simple de la même tâche. Ou, comme nous le verrons bientôt,
traiter certaines structures de données. <br />

Lorsqu'une fonction résout une tâche, elle peut appeler de nombreuses autres
fonctions. Cela se produit partiellement lorsqu'une fonction s'appelle
elle-même. Cela s'appelle la récursion. <br />


### Deux façons de penser #

Commençons par quelque chose de simple - écrivons une fonction pow (x, n) qui
élève x à une puissance naturelle de n. En d'autres termes, multiplie x par
lui-même n fois. <br />

>La fonction Math.pow () en JavaScript permet d’alimenter un nombre, c’est-à-dire la valeur d’un nombre élevé à un exposant. <br />
>**Syntax:** 
>```
>Math.pow(base, exponent)
>```
>**Parameters:** this function accepts two parameters which is mentioned avobe and described below: <br />
>1.base: It is the base number which is to be raised. <br />
>2.exponent: It is the value used to raise the base. <br />
>**Return Value:** The Math.pow() function returns a number representing the given base raised to the power of the given exponent. <br />

```
pow(2, 2) = 4
pow(2, 3) = 8
pow(2, 4) = 16
```

Il y a deux façons d’implémenter cela. <br />

#### 1. Pensée itérative: la boucle for (the for loop) :

```
function pow(x, n) {
  let result = 1;
  // multiply result by x n times in the loop
  for (let i = 0; i < n; i++) {
    result *= x;
  }
  return result;
}

alert( pow(2, 3) ); // 8
```

#### 2. Pensée récursive : simplifiez la tâche et appelez vous-même:

```
function pow(x, n) {
  if (n == 1) {
    return x;
  } else {
    return x * pow(x, n - 1);
  }
}

alert( pow(2, 3) ); // 8
```

Veuillez noter en quoi la variante récursive est fondamentalement différente. <br />
Lorsque pow (x, n) est appelé, l'exécution se scinde en deux branches:

```
            if n==1  = x
           /
pow(x, n) =
           \
            else     = x * pow(x, n - 1)
```

- Si n == 1, alors tout est trivial. On l'appelle la base de la récursion _(base of the recursion)_, car elle produit immédiatement le résultat évident: pow(x, 1) est égal à x.
- Sinon, nous pouvons représenter pow(x, n) par x * pow(x, n - 1). En maths, on écrirait xn = x * xn-1. C'est ce qu'on appelle une étape récursive: nous transformons la tâche en une action plus simple (multiplication par x) et en un appel plus simple de la même tâche (pow avec n inférieur). Les prochaines étapes le simplifient de plus en plus jusqu’à ce que n atteigne 1. <br />
On peut aussi dire que pow s’appelle récursivement jusqu’à n == 1. <br />

![ALT](recursion1.png)

Par exemple, pour calculer pow (2, 4), la variante récursive effectue les étapes suivantes:

1. pow(2, 4) = 2 * pow(2, 3)
2. pow(2, 3) = 2 * pow(2, 2)
3. pow(2, 2) = 2 * pow(2, 1)
4. pow(2, 1) = 2

Ainsi, la récursivité réduit un appel de fonction à un processus plus simple, puis - à un processus encore plus simple, etc. jusqu'à ce que le résultat devienne évident. <br />

**La récursion est généralement plus courte.**
Une solution récursive est généralement plus courte qu'une solution itérative. <br />
Ici, nous pouvons réécrire la même chose en utilisant l'opérateur conditionnel **?** au lieu de **if** faire _pow(x, n)_ plus concis et toujours très lisible:

```
function pow(x, n) {
  return (n == 1) ? x : (x * pow(x, n - 1));
}
```
Le nombre maximal d'appels imbriqués (y compris le premier) est appelé profondeur de récursivité _(recursion depth)_. Dans notre cas, ce sera exactement n. <br />
La profondeur maximale de récursivité est limitée par le moteur JavaScript. Nous pouvons nous assurer que 10000, certains moteurs en autorisent plus, mais 100000 est probablement hors limite pour la majorité d'entre eux. Il existe des optimisations automatiques qui aident à atténuer ce problème (optimisations d'appels en aval = tail calls optimizations), mais elles ne sont pas encore prises en charge partout et ne fonctionnent que dans des cas simples. <br />
Cela limite l'application de la récursivité, mais cela reste très large. Il y a beaucoup de tâches pour lesquelles la pensée récursive donne un code plus simple, plus facile à gérer. <br />


### Le contexte d'exécution (execution context) et la pile (stack) #

Voyons maintenant comment fonctionnent les appels récursifs. Pour cela, nous allons regarder sous le capot des fonctions. <br />
Les informations sur le processus d'exécution d'une fonction en cours d'exécution sont stockées dans son contexte d'exécution. <br />
Le contexte d’exécution est une structure de données interne contenant des détails sur l’exécution d’une fonction : où se trouve le flux de contrôle (control flow), les variables actuelles, sa valeur (nous ne l’utilisons pas ici) et quelques autres détails internes. <br />
Un appel de fonction (function call) est associé à exactement un contexte d'exécution. <br />
Lorsqu'une fonction effectue un appel imbriqué (nested call), les événements suivants se produisent: <br />
- La fonction en cours est suspendue.
- Le contexte d'exécution qui lui est associé est mémorisé dans une structure de données spéciale appelée pile de contexte d'exécution (execution context stack).
- L'appel imbriqué s'exécute.
- Une fois terminé, l'ancien contexte d'exécution est extrait de la pile et la fonction externe reprend à partir de son point d'arrêt. <br />

Voyons ce qui se passe pendant l’appel pow(2, 3). <br />

#### pow(2, 3) #

Au début de l'appel pow(2, 3), le contexte d'exécution stockera les variables: x = 2, n = 3, le flux d'exécution est à la ligne 1 de la fonction. <br />
Nous pouvons l'esquisser comme : <br />
**```Context: { x: 2, n: 3, at line 1 }```** call pow(2, 3) <br />
C'est à ce moment que la fonction commence à s'exécuter. La condition n == 1 étant fausse, le flux se poursuit dans la deuxième branche de if :

```
function pow(x, n) {
  if (n == 1) {
    return x;
  } else {
    return x * pow(x, n - 1);
  }
}

alert( pow(2, 3) );
```

Les variables sont les mêmes, mais la ligne change, le contexte est donc le suivant : <br />
**```Context: { x: 2, n: 3, at line 5 }```**  call pow(2, 3) <br />
Afin de calculer x * pow(x, n - 1), nous avons besoin de creer un sous-appel de pow avec de nouveaux arguments pow(2, 2). <br />

#### pow(2, 2) #

Pour effectuer un appel imbriqué, JavaScript se souvient du contexte d'exécution actuel dans la pile de contextes d'exécution. <br />
Ici, nous appelons la même fonction pow, mais cela n’a absolument aucune importance. Le processus est le même pour toutes les fonctions : <br />
- Le contexte actuel est «mémorisé» au sommet de la pile.
- Le nouveau contexte est créé pour le sous-appel.
- Lorsque le sous-appel est terminé, le contexte précédent est extrait de la pile et son exécution se poursuit. <br />
Voici la pile de contexte lorsque nous sommes entrés dans le sous-appel pow(2, 2):
- **```Context: { x: 2, n: 2, at line 1 }```** call pow(2, 2)
- ```Context: { x: 2, n: 3, at line 5 }``` call pow(2, 3)
Le nouveau contexte d'exécution actuel est en haut (et en gras) et les contextes précédemment mémorisés sont en dessous. <br />
Lorsque nous avons terminé le sous-appel, il est facile de reprendre le contexte précédent, car il conserve les deux variables et l'emplacement exact du code à l'endroit où il s'est arrêté. Ici, sur la photo, nous utilisons le mot "ligne", mais bien sûr, il est plus précis. <br />

#### pow(2, 1) #

Le processus se répète: un nouveau sous-appel est effectué à la ligne 5, avec les arguments x = 2, n = 1. <br />
Un nouveau contexte d'exécution est créé, le précédent est placé en haut de la pile :
- **```Context: { x: 2, n: 1, at line 1 }```** call pow(2, 1)
- ```Context: { x: 2, n: 2, at line 5 }``` call pow(2, 2)
- ```Context: { x: 2, n: 3, at line 5 }``` call pow(2, 3)
Il y a 2 anciens contextes et 1 en cours d'exécution pour pow(2, 1). <br />

### La sortie (the exit) #

Lors de l'exécution de pow(2, 1), contrairement à avant, la condition **n == 1** est verifiee, ainsi la première branche de **if** fonctionne :

```
function pow(x, n) {
  if (n == 1) {
    return x;
  } else {
    return x * pow(x, n - 1);
  }
}
```

Il n'y a plus d'appels imbriqués, donc la fonction se termine et renvoie 2. <br />
Lorsque la fonction se termine, son contexte d’exécution n’est plus nécessaire, elle est donc supprimée de la mémoire. La précédente est restaurée en haut de la pile: <br />
- **```Context: { x: 2, n: 2, at line 5 }```** call pow(2, 2)
- ```Context: { x: 2, n: 3, at line 5 }``` call pow(2, 3)
L'exécution de pow(2, 2) est reprise. Il a le résultat du sous-appel pow(2, 1), de sorte qu'il peut également terminer l'évaluation de x * pow(x, n - 1), renvoyant 4. <br />
Ensuite, le contexte précédent est restauré : <br />
**```Context: { x: 2, n: 3, at line 5 }```** call pow(2, 3) <br />
Quand cela est finit, nous avons un résultat de pow(2, 3) = 8. <br />

La profondeur de la recursion (recursion depth) dans ce cas était : **3**. <br />
Comme nous pouvons le voir dans les illustrations ci-dessus, la profondeur de la recursion est égale au nombre maximal de contextes dans la pile.
Notez les besoins en mémoire. Les contextes prennent de la mémoire. Dans notre cas, augmenter la puissance de n nécessite en fait de la mémoire pour n contextes, pour toutes les valeurs inférieures de n.

Un algorithme basé sur des boucles (loop) est plus économe en mémoire :

```
function pow(x, n) {
  let result = 1;
  for (let i = 0; i < n; i++) {
    result *= x;
  }
  return result;
}
```

La puissance itérative (ireative pow}) utilise un seul contexte qui change et aboutit au processus. Ses besoins en mémoire sont faibles, fixes et ne dépendent pas de n. <br />
Toute récursion peut être réécrite sous forme de boucle. La variante de boucle peut généralement être rendue plus efficace. <br />

… Mais parfois, la réécriture n'est pas triviale, en particulier lorsque la fonction utilise différents sous-appels récursifs en fonction des conditions et fusionne les résultats ou lorsque la création de branche est plus complexe. Et l'optimisation risque de ne pas être nécessaire et de ne pas valoir la peine. <br />
La recursion peut donner un code plus court, plus facile à comprendre et à supporter. Les optimisations ne sont pas nécessaires partout, nous avons surtout besoin d’un bon code, c’est pourquoi il est utilisé. <br />