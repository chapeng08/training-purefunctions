const buildTree = (list, parent) => // parent is the root of the tree/subtree
    return list
	.filter(p => p.parent === parent)
	.reduce((children, child) => {
		children[child.id] = buildTree(list, child.id);
		return children;
	},{});
}

                    
// { autofold
module.exports = {
  buildTree: buildTree
};
// }
