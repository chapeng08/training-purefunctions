La programmation fonctionnelle est l’une de ces choses dont vous avez 
probablement entendu parler, mais que vous n’avez jamais abordée, car elle 
semblait trop dure ou trop théorique. N'attends plus! Dans ce cours, je vous 
présenterai la programmation fonctionnelle à l'aide d'exemples et d'exercices 
dans un langage familier: JavaScript.
À titre de référence, voici les sujets dont nous allons parler:

     Fonctions pures
     Fonctions d'ordre supérieur
     Currying
     Récursion
     Foncteurs
     Monades

Vous devez connaître l'utilisation de base de JavaScript. si vous n'avez jamais 
programmé en JavaScript, vous pouvez vérifier cette introduction rapide et 
revenir, je vous attends.
Open Source est <3, alors voici le référentiel pour ce terrain de jeu, n'hésitez
pas à contribuer, à cloner, ...



    **Fonctions pures**

Je l'avoue, je vous ai menti: nous avons besoin d'un petit peu de théorie pour 
commencer.
Un concept clé de la programmation fonctionnelle est que les fonctions ne 
doivent pas avoir d’effets secondaires et ne doivent pas dépendre d’un état 
externe, c’est-à-dire qu’une fonction doit prendre une entrée et renvoyer une 
sortie sans modifier ni accéder à une valeur extérieure à la fonction.

Si appeler une fonction sans utiliser sa valeur de retour n'a de sens, c'est un 
signe que la fonction n'est pas pure; vous ne feriez jamais cela avec une 
fonction pure.
Par exemple, ceci est une fonction pure:

function add2 (x){
  return x + 2
}

Et ceci ne l'est pas:

var y = 2
function adder (x){
  return x + y
}

Le grand avantage des fonctions pures est que leur sortie est déterministe: avec
une entrée, elle retournera toujours la même valeur. Cette caractéristique les 
rend extrêmement faciles à déboguer. Par exemple, étant donné l'entrée 12, notre
fonction add2 ci-dessus renverra toujours 14.



    **Fonctions d'ordre supérieur**

Les fonctions d'ordre supérieur sont des fonctions qui prennent d'autres 
fonctions en tant que paramètres, Mind Blown.
Nous allons découvrir comment ces fonctions sont utiles à travers quelques 
exemples; en particulier, nous allons recréer la logique de la caisse Amazon.


        **Filter**

Le filter est une méthode de tableaux. Il accepte comme argument une fonction de
test qui doit renvoyer un booléen et renvoie un nouveau tableau avec uniquement 
les éléments pour lesquels la fonction de test a renvoyé la valeur true.

Exemple : Obtenir les nombres pairs dans un tableau

function isEven(x){
  return x % 2 === 0;
}

const numbers = [12,324,213,4,2,3,45,4234];
const evenNumbers = numbers.filter(isEven);
console.log(evenNumbers);

Resultat:Standard Output
[ 12, 324, 4, 2, 4234 ]

Comme vous pouvez le constater, la fonction isEven n'a pas besoin d'inclure la 
logique pour gérer les tableaux. C'est la grande chose à propos des fonctions 
d'ordre supérieur: la logique de décision est séparée de la fonction qui 
l'applique, afin que nous puissions la réutiliser.

Essayons de mettre en œuvre notre première logique pour le panier amazonien: 
extraire du panier tous les éléments principaux:
Implémentez `isPrime` et` primeItems`, pour ce dernier utilisez `filter`.

var cart=[
  {"name":"Biscuits", "type":"regular", "category":"food", "price": 2.0},
  {"name":"Monitor", "type":"prime", "category":"tech", "price": 119.99},
  {"name":"Mouse", "type":"prime", "category":"tech", "price": 25.50},
  {"name":"dress", "type":"regular", "category":"clothes", "price": 49.90},
]

function isPrime(item){
  return item.type === "prime";
}

function primeItems(cart){
  return cart.filter(isPrime);
}

Resultat : Standard Output 
    tests:  should return the prime items: 
  ✓ tests:  should return the prime items: 2ms
    tests:  should use filter: 
  ✓ tests:  should use filter: 0ms

  2 passing (9ms)
  
    
        **Reject**

Nous pouvons maintenant réutiliser la fonction isPrime en conjonction avec 
reject pour obtenir tous les articles non prioritaires dans le panier. La 
fonction reject est l'opposé du filter: elle crée un tableau avec tous les 
éléments sauf ceux qui remplissent la condition.

Reject n'est pas une fonction intégrée à js, nous allons utiliser la 
bibliothèque underscore.js pour l'avoir. La syntaxe est légèrement différente: 
_.reject (list, testFunction) où _ est la bibliothèque underscore.


        **Fonctions Lambda**

Lors de la définition de fonctions courtes, il est souvent pratique d'utiliser 
une syntaxe alternative appelée fonction lambda, qui nous permet de définir des 
fonctions anonymes de manière plus compacte: 
(/ * arguments * /) => {/ * code * /}. 
Si notre fonction est uniquement une instruction return, nous pouvons même 
supprimer les accolades et éviter d'écrire return: 
(/ * arguments * /) => / * valeur à renvoyer * /.
Nous pouvons réécrire le fragment isEven d’avant avec une fonction lambda:

const numbers = [12,324,213,4,2,3,45,4234];
const isEven = (x) => x % 2 === 0
const evenNumbers = numbers.filter(isEven);
console.log(evenNumbers);

Resultat : Standard Output

[ 12, 324, 4, 2, 4234 ]


        ***Map***

Map est une autre fonction très utile d'ordre supérieur: elle prend une fonction
et l'applique à tous les éléments d'un tableau.
La syntaxe est identique à filter

Squaring tous les éléments d'un tableau

const numbers = [1,2,3,4,5,6,10,20];
const squares = numbers.map((x) => Math.pow(x,2)); // 
console.log(squares);

Resultat : Standard Output

[ 1, 4, 9, 16, 25, 36, 100, 400 ]

Revenons maintenant à notre exemple sur Amazon: nous pouvons utiliser Map pour 
appliquer un coupon. La fonction applyCoupon devrait appliquer une réduction de 
20% sur tous les éléments technologiques.
Implémenter la fonction applyCoupon :


        **Reduce**

Reduce est la dernière fonction d'ordre supérieur dont nous allons parler et 
c'est aussi le plus puissant: en fait, vous pouvez implémenter n’importe quelle 
transformation de liste avec reduce.
Reduce prend une fonction de rappel et une valeur de départ, la fonction de 
rappel prend comme arguments un accumulateur et la valeur de l'élément en cours 
du array et renvoie l'accumulateur à utiliser lors du cycle suivant. La valeur
renvoyé du dernier appel de la fonction de rappel est la valeur renvoyée par
reduce.

C'est plus facile si je vous montre quelques exemples:



    **Currying**

Le currying est la technique de traduction de l'évaluation d'une fonction qui 
prend plusieurs arguments pour évaluer une séquence de fonctions, chacune avec 
un seul argument.
Cela devient très facile avec les fonctions et les fermetures lambda. Ouvrons 
directement dans le code:
Obtenir les nombres pairs dans un tableau :

// standard way
function add(x,y,z){
  return x + y + z;
}
console.log("standard: ", add(1,2,3));

// curried style
const add2 = x =>             /* if you have only one argument in the lambda 
function you can omit the parenthesis */
              y =>
                z =>
                  x + y + z;
                  
console.log("curried: ", add2(1)(2)(3) );

Alors pourquoi est-ce utile? Parce que nous sommes maintenant en mesure de 
passer les arguments à différents moments; cela signifie que nous pouvons 
utiliser le curry pour "construire" une fonction.

Rappel de filtre générique pour vérifier toute propriété d'un objet :

const checkProperty = propertyName =>
                      expectedValue =>
                        object =>
                          object[propertyName] === expectedValue;

const candies = [
  {"soft":true, "flavour":"strawberry"},
  {"soft":false, "flavour":"strawberry"},
  {"soft":false, "flavour":"cherry"},
  {"soft":true, "flavour":"orange"},
  {"soft":false, "flavour":"lemon"},
];

const softCandies = candies.filter( checkProperty("soft")(true) ); 
// checkProperty("soft")(true) returns a function that accepts an object, 
exactly what we need

console.log(softCandies);

const strawberryCandies = candies.filter( checkProperty("flavour")("strawberry")
);

console.log(strawberryCandies);

Standard Output

[ { soft: true, flavour: 'strawberry' },
  { soft: true, flavour: 'orange' } ]
[ { soft: true, flavour: 'strawberry' },
  { soft: false, flavour: 'strawberry' } ]

Et maintenant, il est temps de vous salir les mains: vous souvenez-vous de la 
fonction applyCoupon que nous avons décrite dans le chapitre précédent? C'était 
très spécifique; maintenant nous voulons créer une fonction currying qui prend 
comme arguments (dans cet ordre) la catégorie remise entre 0 et 1 (un article de 
2 $ avec une remise de 0.1 coûtera 1,8 $) et un article, et qui renvoie 
l’article avec le prix correct.


    **Recursion**

Une fonction recursion est une fonction qui s’appelle jusqu’à atteindre un cas 
de base. Ceci est souvent utilisé dans les langages récursifs pour remplacer 
les boucles, car il est beaucoup plus flexible. La clé ici est que chaque fois 
que la fonction s’appelle, le problème est réduit.
Juste pour comprendre le concept, implémentons un compte à rebours en tant que 
fonction récursive:

Compte à rebours Countdown ;

let countdown = (num) => {
    if (num === 0) { // base case
        console.log("BOOM");
    }
    else {
        console.log(num);
        countdown(num-1); // recursive call on a reduced problem: num-1
    }
}

countdown(5);

Standard Output

5
4
3
2
1
BOOM

Recursion est beaucoup plus puissante que les boucles; par exemple, ils 
sont très utiles pour implémenter n'importe quel algorithme de la famille 
"diviser pour régner". Nous allons implémenter un algorithme simple nous 
indiquant si un fichier se trouve dans un répertoire ou dans des 
sous-répertoires :