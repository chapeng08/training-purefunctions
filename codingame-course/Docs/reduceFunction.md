**JavaScript Array reduce() Method**


    **Definition and Usage**

The reduce() method reduces the array to a single value.
The reduce() method executes a provided function for each value of the array 
(from left-to-right).
The return value of the function is stored in an accumulator (result/total).

Note: reduce() does not execute the function for array elements without values.
Note: this method does not change the original array.


    **Syntax**

array.reduce(function(total, currentValue, currentIndex, arr), initialValue)


    **Parameters Values**

(Parameters} 

1.  function(total,currentValue, index,arr)     Required. A function to be run 
                                                for each element in the array.

2.  initialValue                                Optional. A value to be passed to 
                                                the function as the initial value.

{Arguments}
1.  total 	            Required. The initialValue, or the previously returned 
                        value of the function.
2.  currentValue 	    Required. The value of the current element.
3  currentIndex 	    Optional. The array index of the current element.
4.  arr 	            Optional. The array object the current element belongs
                        to.