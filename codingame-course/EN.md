Functional programming is one of those things that you probably heard about, but
never approached, because it looked too hard or too theoretical. Wait no more! 
In this course, I'll introduce you to functional programming through examples 
and exercises in a familiar language: JavaScript.
As a reference, here are the topics we'll talk about:

    Pure functions
    Higher Order Functions
    Currying
    Recursion
    Functors
    Monads

You need to know basic JavaScript usage; if you never programmed in JavaScript, 
you can checkout this quick intro and come back, I'll wait for you.
Open Source is <3, so here is the repository for this playground, feel free to 
contribute, clone, ...


    
    **Pure Functions**

I admit it, I lied to you: we need a tiny bit of theory to get started.
One key concept of functional programming is that functions should not have 
side-effects and should not depend on external state, i.e. a function should 
take some input and return some output without modifying or accessing any value 
outside the function.

If calling a function without using its return value makes sense, it is a sign 
that the function is not pure; you would never do that with a pure function.
For example, this is a pure function:

function add2 (x){
  return x + 2
}

And this is not:

var y = 2
function adder (x){
  return x + y
}

The great benefit of pure functions is that their output is deterministic: given
an input it will always return the same value. This characteristic makes them 
extremely easy to debug. For example, given the input 12, our add2 function 
above will always return 14.



    **Higher Order Functions**

Higher Order Functions are functions that take other functions as parameters, 
Mind Blown 💥.
We are going to discover how these functions are useful through some examples; 
in particular, we are going to recreate the logic of the Amazon checkout.


        **Filter**

Filter is a method of arrays. It accepts as argument a test function which 
should return a boolean, and returns a new array with only the elements for 
which the test function returned true.

Example:Get the even numbers in an array

function isEven(x){
  return x % 2 === 0;
}

const numbers = [12,324,213,4,2,3,45,4234];
const evenNumbers = numbers.filter(isEven);
console.log(evenNumbers);

Standard Output
[ 12, 324, 4, 2, 4234 ]

As you can see, the function isEven doesn't need to include the logic to handle 
arrays. This is the great thing about higher order functions: the decision logic
is kept separate from the function applying it, so we can reuse it.

Let's try to implement our first piece of logic for the amazon cart: extracting 
from the cart array all the prime items:
Implement `isPrime` and `primeItems`, for the latter use `filter`.

var cart=[
  {"name":"Biscuits", "type":"regular", "category":"food", "price": 2.0},
  {"name":"Monitor", "type":"prime", "category":"tech", "price": 119.99},
  {"name":"Mouse", "type":"prime", "category":"tech", "price": 25.50},
  {"name":"dress", "type":"regular", "category":"clothes", "price": 49.90},
]

function isPrime(item){
  return item.type === "prime";
}

function primeItems(cart){
  return cart.filter(isPrime);
}

Standard Output 
    tests:  should return the prime items: 
  ✓ tests:  should return the prime items: 2ms
    tests:  should use filter: 
  ✓ tests:  should use filter: 0ms

  2 passing (9ms)


        **Reject**

We can now reuse the isPrime function in conjunction with reject to get all the
non-prime items in the cart. The reject function is the opposite of the filter: 
it creates an array with all the elements but those that satisfy the condition.

Reject is not a built-in function in js, we are going to use the library 
underscore.js to have it. The syntax is slightly different: 
_.reject(list, testFunction) where _ is the underscore library.



        **Lambda functions**

When defining short functions, it's often convenient to use an alternative 
syntax called lambda function that allows us to define anonymous functions in a 
more compact way: ( /*arguments*/ ) => { /*code*/ }. If our function is only a 
return statement, we can even strip the curly brackets and avoid writing return:
( /*arguments*/ ) => /*value to return*/.

We can rewrite the isEven snippet from before with a lambda function:
Get the even numbers in an array

const numbers = [12,324,213,4,2,3,45,4234];
const isEven = (x) => x % 2 === 0
const evenNumbers = numbers.filter(isEven);
console.log(evenNumbers);

Resul:Standard Output

[ 12, 324, 4, 2, 4234 ]


        **Map**

Another very useful higher order function is map: it takes a function and 
applies it to all the elements of an array.
The syntax is identical to filter

Squaring all the elements of an array

const numbers = [1,2,3,4,5,6,10,20];
const squares = numbers.map((x) => Math.pow(x,2)); // 
console.log(squares);

Resul:Standard Output

[ 1, 4, 9, 16, 25, 36, 100, 400 ]

Now back to our amazon example: we can use map to apply a coupon. The 
applyCoupon function should apply a 20% discount on all the tech items.
Implement the applyCoupon function



        **Reduce**

Reduce is the last higher order function we are going to discuss and it's also 
the most powerful: in fact, you can implement any list transformation with 
reduce.
Reduce takes in a callback function and a starting value, the callback function
takes as arguments an accumulator and the value of the current element of the 
array and returns the accumulator to be used in the next cycle. The value 
returned from the last call of the callback function is the value returned by 
reduce.

It's easier if I show you some examples:



    Currying

Currying is the technique of translating the evaluation of a function that takes
multiple arguments into evaluating a sequence of functions, each with a single 
argument.
This becomes very easy using lambda functions and closures. Let's dive straight 
into code:
Get the even numbers in an array

// standard way
function add(x,y,z){
  return x + y + z;
}
console.log("standard: ", add(1,2,3));

// curried style
const add2 = x =>             /* if you have only one argument in the lambda 
function you can omit the parenthesis */
              y =>
                z =>
                  x + y + z;
                  
console.log("curried: ", add2(1)(2)(3) );

So why is this useful? Because we are now able to pass the arguments at
different points in time; this means that we can use currying to "construct" a 
function.

Generic filter callback to check any property of an object:

const checkProperty = propertyName =>
                      expectedValue =>
                        object =>
                          object[propertyName] === expectedValue;

const candies = [
  {"soft":true, "flavour":"strawberry"},
  {"soft":false, "flavour":"strawberry"},
  {"soft":false, "flavour":"cherry"},
  {"soft":true, "flavour":"orange"},
  {"soft":false, "flavour":"lemon"},
];

const softCandies = candies.filter( checkProperty("soft")(true) ); 
// checkProperty("soft")(true) returns a function that accepts an object, 
exactly what we need

console.log(softCandies);

const strawberryCandies = candies.filter( checkProperty("flavour")("strawberry") 
);
console.log(strawberryCandies);

Standard Output

[ { soft: true, flavour: 'strawberry' },
  { soft: true, flavour: 'orange' } ]
[ { soft: true, flavour: 'strawberry' },
  { soft: false, flavour: 'strawberry' } ]

And now, time to get your hands dirty: remember the applyCoupon function we 
wrote in the previous chapter? It was very specific; now we want to create a 
curryable function that takes as arguments (in this order) category, discount 
between 0 and 1 (a 2$ item with a 0.1 discount will cost 1.8$) and an item, and 
that returns the item with the correct price.


Recursion

A recursive function is a function that calls itself until it hits a base case.
This is often used in recursive languages as a replacement for loops, because it
is far more flexible. They key here is that each time the function calls itself,
the problem is reduced.

Just to understand the concept, let's implement a countdown as a recursive 
function:

Countdown

let countdown = (num) => {
    if (num === 0) { // base case
        console.log("BOOM");
    }
    else {
        console.log(num);
        countdown(num-1); // recursive call on a reduced problem: num-1
    }
}

countdown(5);

Standard Output

5
4
3
2
1
BOOM

Recursion is much more powerful than loops; for example, they are very useful to
implement any algorithm of the "divide and conquer" family. We'll implement a 
simple algorithm that tells us whether a file is in a directory or in 
sub-directories.