La récursion est un gros mot effrayant que vous entendez souvent à propos de la
programmation, en particulier du type de programmation frustrant qu’ils
enseignent à l’université. Bien que ce soit un concept facile à décrire, c’est
vraiment époustouflant de comprendre comment fonctionne la récursion. La
plupart des gens l'acceptent et passent à autre chose. Pas ici!

La récursion est fondamentalement le processus d'une fonction qui s'appelle
elle-même. Par exemple:

void funct(int x)
{
    funct(x);
}

Dans ce morceau de code, vous voyez un exemple terrible d’une fonction
récursive, mais il sert ici à des fins d’illustration: La fonction funct ()
s’appelle elle-même. C’est la récursion. Maintenant, ce qui se passe dans cet
exemple est fondamentalement une boucle sans fin, et, grâce à quelque chose de
technique, appelé le pointeur de pile (stack pointer), l'ordinateur finit par
tomber en panne. Mais ce n’est qu’une illustration.

Pour que la récursion fonctionne, la fonction doit avoir une condition de
sauvetage (bailout condition), tout comme une boucle. Par conséquent, la valeur
transmise à la fonction récursive ou sa valeur de retour doit être testée.
Voici un meilleur exemple de fonction récursive:

void recursion(int x)
{
    if(x==0)
        return;
    else
    {
        puts("Boop!");
        recursion(--x);
    }
}

La fonction récursion () accepte la valeur x. Si x est égal à zéro, la fonction
échoue. Sinon, la fonction est appelée à nouveau, mais la valeur de x est
réduite. L'opérateur de préfixe de décrémentation est utilisé pour que la valeur
de x soit réduite avant que l'appel soit effectué.

La fonction exemple de récursion () recrache le texte Boop! un nombre de fois
donné. Donc, si récursion () est appelée avec la valeur 10, vous voyez ce texte
affiché dix fois.

La partie insensée de la récursion est que la fonction continue à s’appeler, se
resserrant de plus en plus, comme si elle était dans une spirale. Dans l'exemple
précédent, la condition x == 1 résout finalement ce gâchis sinueux, en se
rétractant de plus en plus jusqu'à ce que la fonction soit terminée.

Le code suivant montre un programme complet utilisant la fonction exemple
recursion ().

#include <stdio.h>
void recursion(int x);
int main()
{
    recursion(10);
    return(0);
}
void recursion(int x)
{
    if(x==0)
        return;
    else
    {
        puts("Boop!");
        recursion(--x);
    }
}

Une démonstration courante de la récursion est une fonction factorielle. La
factorielle est le résultat de la multiplication d'une valeur par chacun de ses
entiers positifs. Par exemple:

4! = 4 × 3 × 2 × 1

Le résultat de cette factorielle est 24. L'ordinateur peut également effectuer
ce calcul, en implémentant une boucle ou en créant une fonction récursive. Voici
une telle fonction:

int factorial(int x)
{
    if(x==1)
        return(x);
    else
        return(x*factorial(x-1));
}


Comme avec les autres fonctions récursives, la fonction factorial () contient
une condition de sortie: x == 1. Sinon, la fonction est appelée à nouveau avec
un moins que la valeur actuelle de x. Mais toute l'action se déroule avec les
valeurs de retour.
