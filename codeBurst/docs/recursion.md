## Apprendre et comprendre la recursion? #

La recursion se produit lorsqu'une fonction s'appelle elle-meme. <br />
L'exemple le plus celebre :

```
function factorial(x) {
  if (x < 0) return; // Termination. Impossible de factoriser un nombre negatif.
  if (x === 0) return 1; // Base case. Notre but : une fois x reduit a 0, nous avons reussi a determiner notre factorielle.
  return x * factorial(x - 1); // Recursion. On renvoie la valeur de x multipliee par la valeur de tout ce qui est factorial(x-1).
}
factorial(3)
```

### Les trois caracteristiques qui composent la recursion : #

1. Une condition de resiliation **(termination condition)** <br />
if(something bad happened){STOP}; <br />
Frein d`urgence en cas de mauvaise entree. Ce qui pourrait empecher la recursion de fonctionner. <br />
2. Un scenario de base **(base case)** <br />
if(this happens) {Yay! We`re done}; <br />
Similaire a termination car peut arreter notre recursion. Mais ici base case est le but de notre recursion alors que la premiere est un fourre-tout pour les mauvaises entrees. <br />
3. **Recursion**
La fonction s`appelle elle-meme.


### Flux de fonction factorielle (factorial function flow) #

La recursion est simplement un groupe d`appels de fonctions imbriquees.


### 2nd exemple : inversion de chaine (reversing a string) #

```
function revStr(str) {
  if (str === '') return ''; // (base case) lorsque la chaine ne contient aucun caractere nous avons reussi.
  return revStr(str.substr(1)) + str[0]; // (recursion)
}
revStr('cat'); //tac
```
Dans cet exemple, pas de condition de resiliation (termination) car ici notre base case est notre condition de resiliation. On ne peut obtenir une chaine avec des caracteres negatifs. <br />
Explication de l'exemple (inverser la chaine) ligne par ligne (dans l'ordre) :

1. revStr('cat')
Nous commencons par un appel de notre fonction, en passant la chaine cat. En JS, la fonction substr() est une methode qui retourne une chaine en commencant a l'emplacement specifie (ici 1 donc le premier). Ainsi
cat.substr(1) === 'at'. Et str[0] qui est cat[0] === 'c'
2. return revStr(str.substr(1)) + str[0];
Cela revient a
return revtr('at') + 'c'
Notre cas de recursion est execute.
3. return revStr(str.substr(1)) + str[0];
Cela revient a
return revtr('t') + 'a'
Notre cas de recursion est execute.
4. return revStr(str.substr(1)) + str[0];
Cela revient a
return revtr('') + 't'
ici notre base case est execute et la fonction retourne une chaine vide **(blank string)**.
5. if (str === '') return '';
Maintenant que notre fonction est revenue et notre but atteint, tout va se derouler et revenir dans l'ordre.
6. return '' + 't' + 'a' + 'c' // tac

Decomposons encore plus cet exemple :

revStr('cat') returns revStr('at') + 'c' <br />
revStr('at') returns revStr('t') + 'a' <br />
revStr('t') returns revStr('') + 't' <br />
revStr('') returns '' <br />

Ce sont toutes des appels de fonctions imbriquees. Donc la fonction imbriquee la plus interne est rentournee en premier et initie le deroulemet a leur retour dans l'ordre.

revStr('') returns '' => '' <br />
revStr('t') returns revStr('') + 't' => '' + 't' <br />
revStr('at') returns revStr('t') + 'a' => '' + 't' + 'a' <br />
revStr('cat') returns revStr('at') + 'c' => '' + 't' + 'a' + 'c' <br />
// tac
