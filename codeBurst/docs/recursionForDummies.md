La récursion est une technique de programmation de base que vous pouvez
utiliser en Java, dans laquelle une méthode s’appelle pour résoudre un problème.
Une méthode qui utilise cette technique est récursive. De nombreux problèmes de
programmation ne peuvent être résolus que par la récursion, et certains
problèmes pouvant être résolus par d'autres techniques sont mieux résolus par la
récursion.

L'un des problèmes classiques d'introduction de la récursion est le calcul de
la factorielle d'un entier. La factorielle d'un entier donné - appelez-le n pour
que vous ayez l'air mathématique - est le produit de tous les entiers compris
entre 1 et n. Ainsi, la factorielle de 5 est 120: 5 x 4 x 3 x 2 x 1.

La manière récursive de regarder le problème factoriel est de se rendre compte
que la factorielle pour un nombre donné n est égale à n fois la factorielle de
n – 1, à condition que n soit supérieur à 1. Si n est 1, la factorielle de n est
1.

Cette définition de factorielle est récursive car elle inclut la méthode
factorielle elle-même. Elle inclut également la partie la plus importante de
toute méthode récursive: une condition de fin. La condition de fin indique quand
la méthode récursive doit cesser de s’appeler. Dans ce cas, lorsque n vaut 1, il
retourne simplement 1. Sans condition de fin, la méthode récursive continue à
s'appeler pour toujours.

Voici la version récursive de la méthode factorielle:

private static long factorial(int n)
{
    if (n == 1)
        return 1;
    else
        return n * factorial(n-1);
}

Aussi...

Une fonction est appelée une fonction récursive si elle s’appelle encore et
encore.
La récursion peut être directe ou indirecte.
	1- La recursion directe se produit lorsqu'une fonction s'appelle elle-même.
	2- La recursion indirecte se produit lorsqu'une fonction appelle une autre
fonction et que la fonction appelée appelle à son tour la fonction appelante.
C'est-à-dire si la fonction 1 appelle la fonction 2, puis la fonction 2
appelle la fonction 1, ce qui entraîne un cycle.

Maintenant, comprenons ce qu'est exactement la récursivité et à quoi sert-elle…
Prenons un exemple. Supposons que nous ayons un mot (problem) qui nous est
inconnu. Nous devons trouver sa signification, nous allons donc la chercher dans
le dictionnaire (function).

Mais lorsque nous examinons le sens, nous constatons que le sens contient à son
tour encore un autre mot que nous ne comprenons pas. Nous avons donc une
solution mais cette solution contient une partie qui reste problématique. Afin
de bien comprendre le sens du mot original, nous devons d'abord comprendre le
sens du mot problématique dans le sens du mot original.
Pour ce faire, nous recherchons à nouveau dans le dictionnaire la signification
du nouveau mot problématique. C'est que nous suivons le même chemin que nous
avons fait dans la première étape.
Nous allons répéter ce processus jusqu'à trouver une solution ne contenant aucun
 mot problématique.

C'est à cela que sert la récursion. Nous avons un problème à résoudre et pour
trouver la solution, nous suivons un certain chemin. Nous obtenons une solution
mais cette solution a encore quelques problèmes.
Afin de trouver la solution complète, nous devons d'abord résoudre la partie
problématique. Pour ce faire, nous suivons le même chemin que nous avons suivi
dans la première étape. C'est appeler la même fonction à nouveau.
Nous le faisons jusqu'à ce que nous trouvions une solution qui nous semble
parfaitement logique. C'est l'étape finale.

Par exemple, voyons un programme pour trouver une factorielle d’un nombre en
utilisant les fonctions récursives :

#include<iostream>

using namespace std;
int factorial(int n);
int main() {
int n;
cout << "Enter a positive integer: "; cin >> n;
cout << "Factorial of " << n << " = " << factorial(n);
return 0;
}
int factorial(int n) {
if(n > 1)
return n * factorial(n - 1);
else return 1;
}

Si ici n est donné une valeur 5. La fonction factorielle sera invoquée pour 5,
la solution que cette fonction proposerait lors du premier passage serait 5 * 4!
Ici, la partie problématique de cette solution est 4!
Donc, la fonction factorielle s’appellera à nouveau mais cette fois pour 4.
Ce processus se répétera jusqu'à ce que nous n'ayons plus de terme comme n!
La dernière fois que la fonction factorielle serait appelée est pour 1! qui
retournera 1 comme réponse, puis en retraçant, nous aurons la solution pour 5!

Remarque: il se peut que nous n'atteignions jamais un cas final. Ceci est connu
comme une récursion infinie.

Dans les exemples de dictionnaire ci-dessus, cela peut être expliqué comme…

Lorsque nous recherchons le sens du mot original, nous trouvons un mot
problématique et lorsque nous recherchons le sens du mot problématique, nous
trouvons à son tour le mot original dans sa définition. C’est-à-dire que nous
continuerons indéfiniment dans un cycle, car il n’y a pas de cas de clôture.
