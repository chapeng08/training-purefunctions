function factorial(x) {
  if (x < 0) return; // Termination 
  if (x === 0) return 1; // Base case
  return x * factorial(x - 1); // Recursion
}

factorial(3)
