function revStr(str) {
  if (str === '') return ''; // (base case) lorsque la chaine ne contient aucun caractere nous avons reussi.
  return revStr(str.substr(1)) + str[0]; // (recursion)
}

revStr('cat');

//tac
